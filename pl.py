import requests
import json
import time

#api to generate auth_token
def auth(email, password):
    url = 'https://api-app.flytnow.com/login/user'
    body = {'email': f'{email}', 'password': f'{password}'}
    response = requests.post(url, data = body)
    print(response.status_code)
    return response.text

#api to get all drone details 
def drone_details(auth):
    url = 'https://api-app.flytnow.com/drone_details/get_all_drones'
    headers = {'Authorization': f'{auth}'}
    response = requests.get(url, headers = headers)
    print(response.status_code)
    return response.text

#api to search drone_id using given vehicle_id
def search_drone_id(auth):
    all_drone_details = json.loads(drone_details(auth))
    all_drone_ids = all_drone_details["data"]
    #print(all_drone_ids)
    #print(all_drone_ids[0]['_id'])
    for x in all_drone_ids:
        if x['vehicle_id'] == vehicle_id:
            return x['_id']

#api to trigger takeoff with alt input
def takeoff_wp(alt, vehicle_id, token):
    url = "https://dev.flytbase.com/rest/ros/flytos/navigation/take_off"
    payload = json.dumps({"takeoff_alt": alt})
    headers = {
               'Authorization': f'Token {token}',
               'VehicleID': vehicle_id,
              }
    response = requests.post(url, headers = headers, data = payload)
    print(response.text)
    #print(response.status_code)
    #print(response.reason)

#api to trigger takeoff with default alt 1.5 m
def takeoff(vehicle_id, token):
    url = "https://dev.flytbase.com/rest/ros/flytos/navigation/take_off"
    payload = json.dumps({"takeoff_alt": 1.2})
    headers = {
               'Authorization': f'Token {token}',
               'VehicleID': vehicle_id,
              }
    response = requests.post(url, headers = headers, data = payload)
    print(response.text)
    #print(response.status_code)
    #print(response.reason)

#api to call velocity setpoint
def velocity_setpoint(vehicle_id, token):
    url = "https://dev.flytbase.com/rest/ros/flytos/navigation/velocity_set"
    payload = json.dumps({
        "vx": 0,
        "vy": 0,
        "vz": 1,
        "yaw_rate": 0,
        "tolerance": 1,
        "async": False,
        "relative": True,
        "yaw_rate_valid": False,
        "body_frame": False
        })
    headers = {
        'Authorization': f'Token {token}',
        'VehicleID': vehicle_id,
        'Content-Type': 'application/json'
        }
    response = requests.post(url, headers = headers, data = payload)
    print(response.text)

#api to call position set point
def position_setpoint(vehicle_id, token):
    url = "https://dev.flytbase.com/rest/ros/flytos/navigation/position_set"
    payload = json.dumps({
        "x": 0,
        "y": 0,
        "z": -2,
        "yaw": 0,
        "tolerance": 1,
        "async": False,
        "relative": True,
        "yaw_valid": False,
        "body_frame": False
        })
    headers = {'Authorization': f'Token {token}', 'VehicleID': vehicle_id, 'Content-Type': 'application/json'}
    response = requests.post(url, headers=headers, data=payload)
    print(response.text)



#api to call pos_hold
def pos_hold(vehicle_id, token):
    url = "https://dev.flytbase.com/rest/ros/flytos/navigation/position_hold"
    headers = {
               'Authorization': f'Token {token}',
               'VehicleID': vehicle_id,
              }
    response = requests.get(url, headers = headers)
    print(response.text)

#api to trigge precision land
def execute_pl(drone_id, auth):
    url = f"https://api-app.flytnow.com/precision_landing/execute/{drone_id}"
    payload = json.dumps({"pl_command": 23})
    headers = {'Authorization': f'{auth}', 'Content-Type': 'application/json'}
    response = requests.post(url, headers=headers, data=payload)
    print(response.text)

#api to call cancel pl
def cancel_pl(drone_id, auth):
    url = f"https://api-app.flytnow.com/precision_landing/cancel/{drone_id}"
    headers = {'Authorization': f'{auth}'}
    response = requests.post(url, headers = headers)
    print(response.text)

#api to check drone status
def drone_status(token, vehicle_id):
    url = "https://dev.flytbase.com/rest/ros/flytos/flyt/state"
    headers = {
               'Authorization': f'Token {token}',
               'VehicleID': vehicle_id,
              }
    response = requests.get(url, headers = headers)
    return response.text

#api to check global position
def altitude(token, vehicle_id):
    url = "https://dev.flytbase.com/rest/ros/flytos/mavros/global_position/global"
    headers = {
               'Authorization': f'Token {token}',
               'VehicleID': vehicle_id,
              }
    response = requests.get(url, headers = headers)
    return response.text


# email = input("email_id: ")
# password = input("password: ")
# vehicle_id = input("vehicle_id: ")
# token = input("personal_access_token: ")
# alt = float(input("altitude: "))
# reps = int(input("reps: "))

email = "dinesh.kumar@flytbase.com"
password = "flytos2022"
vehicle_id = "q8DENfn4"
token = "39c4c0818c6a39e9b8db4eca94d95a87bce5c4cf"
alt = float(10)
reps = 2


for x in range(reps):

    authentication = auth(email,password)
    drone_id = search_drone_id(authentication)
    print(drone_id)
    
    #uncomment bleow two line to take off using take off api
    #takeoff_wp(alt,vehicle_id,token) 
    #time.sleep(15)
    
    takeoff_alt = json.loads(altitude(token, vehicle_id))["altitude"]
    print(takeoff_alt)
    takeoff(vehicle_id, token)

    is_reached = True
    while is_reached == True:
        cur_altitude = json.loads(altitude(token, vehicle_id))
        if cur_altitude["altitude"] >= (takeoff_alt + alt):
            is_reached = False
        velocity_setpoint(vehicle_id, token)
        #position_setpoint(vehicle_id, token)
        print(cur_altitude["altitude"])    
    
    pos_hold(vehicle_id, token)
    execute_pl(drone_id, authentication)


    is_armed = True
    while is_armed == True:
        state = json.loads(drone_status(token, vehicle_id))
        if state["armed"] == False:
            is_armed = False
        print("armed")
        time.sleep(1)










